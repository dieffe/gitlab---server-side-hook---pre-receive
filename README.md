# GitLab
## Server Side Hook

### pre-receive

The first script to run when handling a push from a client is pre-receive. It takes a list of references that are being pushed from stdin; if it exits non-zero, none of them are accepted. You can use this hook to do things like make sure none of the updated references are non-fast-forwards, or to do access control for all the refs and files they’re modifying with the push.

### Configuration

1.  Create a custom_hook inside your project folder

```bash
mkdir /var/opt/gitlab/git-data/repositories/<group>/<project>.git/custom_hooks
```

2.  Grant git ownership

```bash
chown git:git /var/opt/gitlab/git-data/repositories/<group>/<project>.git/custom_hooks
```

3. Create pre-receive file

```bash
touch /var/opt/gitlab/git-data/repositories/<group>/<project>.git/custom_hooks/pre-release
```

4. Grant git ownership and give +x permissions

```bash
chown git:git /var/opt/gitlab/git-data/repositories/<group>/<project>.git/custom_hooks/pre-release
chmod +x /var/opt/gitlab/git-data/repositories/<group>/<project>.git/custom_hooks/pre-release
```

### Check commit messages for JIRA issue keys

```bash
#!/bin/bash
    #
    # check commit messages for JIRA issue numbers

    jiraIdRegex="[a-z]+[0-9]*[a-z]*\-[0-9]+"

    error_msg="[POLICY] The commit doesn't reference a JIRA issue"

    while read oldrev newrev refname
    do
      for sha1Commit in $(git rev-list $oldrev..$newrev);
      do
        echo "sha1 : $sha1Commit";
        commitMessage=$(git log --format=%B -n 1 $sha1Commit)
	echo " "
	echo "Commit message => "$commitMessage
	echo " "
        
	if [[ $commitMessage =~ "this commit message will skip check" ]]
	then
	   	echo " "
		echo "Policy exception detected, skipping JIRA issue check. "
		echo " "
		exit 0
	fi

	jiraIds=$(echo $commitMessage | egrep -ie $jiraIdRegex)

        if [ -z "$jiraIds" ]; then
        	echo " "  
		echo "$error_msg: $commitMessage" >&2
		echo " "
          exit 1
        fi
      done
    done
    exit 0

```